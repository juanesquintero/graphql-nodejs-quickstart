import express from 'express';
import ViteExpress from "vite-express";
import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { typeDefs, resolvers } from "./schemas";

const app = express();
const port = process.env.PORT || 4000;

const bootstrapServer = async () => {

    const server = new ApolloServer({ typeDefs, resolvers })
    await server.start()


    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))
    app.use('/graphql', expressMiddleware(server));
    app.get('/', (req, res) => {
        res.json({
            api: 'ExpressJS & GraphQL'
        })
    });

    ViteExpress.listen(app, port, console.log(`Server listening on http://localhost:${port}`));
};

bootstrapServer()