import crypto from 'crypto';
import { filter, remove, findIndex, merge } from 'lodash';
import courses_list from '../db/courses.json';

export default {
    Query: {
        list() {
            return courses_list;
        },
        course(_, { id }) {
            const course = filter(courses_list, { 'id': id });
            return course ? course[0] : null;
        }
    },
    Mutation: {
        createCourse(_, { input }) {
            input.id = crypto.randomBytes(16).toString('hex');
            courses_list.push(input);
            return input;
        },
        deleteCourse(_, { id }) {
            const course = filter(courses_list, { 'id': id });
            remove(courses_list, { 'id': id });
            return course ? course[0] : null;
        },
        updateCourse(_, { id, input }) {
            const index = findIndex(courses_list, { 'id': id });
            input.id = id;
            courses_list.splice(index, 1, input);
            return courses_list[index];
        },
        patchCourse(_, { id, input }) {
            const index = findIndex(courses_list, { 'id': id });
            const patchedCourse = merge(courses_list[index], input);
            courses_list.splice(index, 1, patchedCourse);
            return courses_list[index];
        }
    }
}