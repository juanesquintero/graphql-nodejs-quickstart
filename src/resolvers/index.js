import helloResolvers from './hellos'
import courseResolvers from './courses'


export {
    helloResolvers,
    courseResolvers
}
